<databaseChangeLog
        xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
             http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.4.xsd">

    <changeSet id="create_users_table" author="oleh.dyriavko">
        <createTable tableName="users">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="username" type="VARCHAR(100)">
                <constraints nullable="false" unique="true"/>
            </column>

            <column name="first_name" type="VARCHAR(100)">
                <constraints nullable="false"/>
            </column>

            <column name="last_name" type="VARCHAR(100)">
                <constraints nullable="false"/>
            </column>

            <column name="email" type="VARCHAR(255)">
                <constraints nullable="false" unique="true"/>
            </column>

            <column name="password" type="VARCHAR(255)">
                <constraints nullable="false"/>
            </column>

            <column name="status" type="VARCHAR(25)" defaultValue="ACTIVE">
                <constraints nullable="false"/>
            </column>

        </createTable>
    </changeSet>

   <changeSet id="create_roles_table" author="oleh.dyriavko">
        <createTable tableName="roles">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="name" type="VARCHAR(100)">
                <constraints nullable="false" unique="true"/>
            </column>
        </createTable>
    </changeSet>

   <changeSet id="create_user_roles_table" author="oleh.dyriavko">
        <createTable tableName="user_roles">
            <column name="user_id" type="BIGINT"/>
            <column name="role_id" type="BIGINT"/>
        </createTable>
    </changeSet>

    <changeSet id="create_categories_table" author="oleh.dyriavko">
        <createTable tableName="categories">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="category_name" type="VARCHAR(255)">
                <constraints nullable="false"/>
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create_orders_table" author="oleh.dyriavko">
        <createTable tableName="orders">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="user_id" type="BIGINT">
                <constraints nullable="false" unique="true"/>
            </column>

            <column name="order_date" type="DATETIME">
                <constraints nullable="false"/>
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create_order_details_table" author="oleh.dyriavko">
        <createTable tableName="order_details">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="order_id" type="BIGINT">
                <constraints nullable="false"/>
            </column>

            <column name="product_id" type="BIGINT">
                <constraints nullable="false"/>
            </column>

            <column name="quantity" type="INT">
                <constraints nullable="false"/>
            </column>

            <column name="sale_price" type="DECIMAL(12, 2)">
                <constraints nullable="false"/>
            </column>

        </createTable>
    </changeSet>

    <changeSet id="create_products_table" author="oleh.dyriavko">
        <createTable tableName="products">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="product_name" type="VARCHAR(255)">
                <constraints nullable="false"/>
            </column>

            <column name="category_id" type="BIGINT">
                <constraints nullable="false"/>
            </column>

            <column name="unit" type="VARCHAR(255)">
                <constraints nullable="false"/>
            </column>

            <column name="quantity" type="INT">
                <constraints nullable="false"/>
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create_delivery_details_table" author="oleh.dyriavko">
        <createTable tableName="delivery_details">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="delivery_id" type="BIGINT">
                <constraints nullable="false"/>
            </column>

            <column name="product_id" type="BIGINT">
                <constraints nullable="false"/>
            </column>

            <column name="quantity" type="INT">
                <constraints nullable="false"/>
            </column>

            <column name="cost_price" type="DECIMAL(12, 2)">
                <constraints nullable="false"/>
            </column>

        </createTable>
    </changeSet>

    <changeSet id="create_deliveries_table" author="oleh.dyriavko">
        <createTable tableName="deliveries">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="provider_id" type="BIGINT">
                <constraints nullable="false" unique="true"/>
            </column>

            <column name="delivery_date" type="DATETIME">
                <constraints nullable="false"/>
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create_providers_table" author="oleh.dyriavko">
        <createTable tableName="providers">
            <column name="id" type="BIGINT" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="provider_name" type="VARCHAR(255)">
                <constraints nullable="false" unique="true"/>
            </column>

            <column name="phone" type="VARCHAR(100)">
                <constraints nullable="false"/>
            </column>

        </createTable>
    </changeSet>

    <changeSet id="add_fk_user_id_in_user_roles" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="user_roles"
                                 baseColumnNames="user_id"
                                 constraintName="fk_user_roles_user"
                                 referencedTableName="users"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>
    <changeSet id="add_fk_role_id_in_user_roles" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="user_roles"
                                 baseColumnNames="role_id"
                                 constraintName="fk_user_roles_roles"
                                 referencedTableName="roles"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>

    <changeSet id="add_fk_order_in_order_details" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="order_details"
                                 baseColumnNames="order_id"
                                 constraintName="fk_order_details_order"
                                 referencedTableName="orders"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>

    <changeSet id="add_fk_product_in_order_details" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="order_details"
                                 baseColumnNames="product_id"
                                 constraintName="fk_order_details_product"
                                 referencedTableName="products"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>

    <changeSet id="add_fk_category_in_products" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="products"
                                 baseColumnNames="category_id"
                                 constraintName="fk_products_category"
                                 referencedTableName="categories"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>

    <changeSet id="add_fk_delivery_in_delivery_details" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="delivery_details"
                                 baseColumnNames="delivery_id"
                                 constraintName="fk_delivery_details_delivery"
                                 referencedTableName="deliveries"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>

    <changeSet id="add_fk_product_in_delivery_details" author="oleh.dyriavko">
        <addForeignKeyConstraint baseTableName="delivery_details"
                                 baseColumnNames="product_id"
                                 constraintName="fk_delivery_details_product"
                                 referencedTableName="products"
                                 referencedColumnNames="id"
                                 onDelete="CASCADE"
                                 onUpdate="RESTRICT"/>
    </changeSet>

</databaseChangeLog>